package org.bitbucket.vmarquet.GardenControlers;

import org.bitbucket.vmarquet.Menu;
import org.bitbucket.vmarquet.Model.*;

import org.apache.log4j.Logger;
import java.util.Scanner;
import java.awt.Dimension;
import java.io.*;

public class GardenReaderControler {
	
	private static final Logger LOGGER = Logger.getLogger(GardenReaderControler.class);
	
	//garden : defined by height and width
	private int width, height;
	
	private GameModel model = null;
	
	public GardenMatrix garden;
	
	//Reads the garden.txt file (which contains all information about the garden)
	//and store it in GardenMatrix garden
	public GardenReaderControler(String filename) {
		
		try{
		 garden = new GardenMatrix(new Dimension(0, 0));
		//Reading the file garden.txt ...
		LOGGER.debug("GardenReaderControler: Opening file " + filename + " for reading");
		LOGGER.debug(filename);
		Scanner scanner = new Scanner(new File(filename));
		LOGGER.info("Reading the garden...");
		
		//... line by line 
		while(scanner.hasNextLine()){
			String line = scanner.nextLine();
			LOGGER.debug(line);
			
			
			
			// Switch on the first letter of the line
			switch(line.charAt(0)) {
				
				//if the first character of the line is a 'J'
				case 'J':
					// we split the line to get the width and the height of the garden
					String[] word = line.split(" ");
					
					if (word.length != 3){
						
						LOGGER.debug("WARNING: line does not follow conventions");
						break;
					}
					else{
					
						width = Integer.parseInt(word[1]);
						height = Integer.parseInt(word[2]);
	
						if(width>0 && height>0){
						
							// we initialize garden with its dimensions
							garden = new GardenMatrix(new Dimension(width, height));
							this.model = GameModel.getInstance();
							this.model.setGardenMatrix(garden);
							break;
						}
						else{
							
							LOGGER.debug("WARNING: line does not follow conventions");
							break;
						}
					}
					
				//if the first character of the line is a 'C' (carrot) ...
				case 'C':
					// we split the line
					String[] wordC = line.split(" ");
					
					if (wordC.length != 3){
						LOGGER.debug("WARNING: line does not follow conventions");
						break;
					}
					
					else{
						// we get the position of the carrot
						String[] pos = wordC[1].split("-");
						int pos_x = Integer.parseInt(pos[0]);
						int pos_y = Integer.parseInt(pos[1]);
						// we get the number of carrots
						int nb = Integer.parseInt(wordC[2]);
						
						//... we add the amount of carrots at the indicated position in the garden
						if(pos_x-1>=0 && pos_x-1<=garden.getGardenSize().getWidth()-1 && pos_y-1>=0 && pos_y-1<=garden.getGardenSize().getHeight()-1){
							for(int i=0; i<nb; i++){	
									garden.addObject(new Dimension(pos_x-1, pos_y-1), GardenObject.CARROT);
							}
						}
						break;
					}
					
				//if the first character of the line is a 'B' (buried carrot) ...
				case 'B':
					// we split the line
					String[] wordB = line.split(" ");
					
					if (wordB.length != 3){
						LOGGER.debug("WARNING: line does not follow conventions");
						break;
					}
					
					else{
						// we get the position of the carrot
						String[] pos2 = wordB[1].split("-");
						int pos2_x = Integer.parseInt(pos2[0]);
						int pos2_y = Integer.parseInt(pos2[1]);
						// we get the number of carrots
						int nb2 = Integer.parseInt(wordB[2]);
						
						//... we add the amount of buried carrots at the indicated position in the garden
						if(pos2_x-1>=0 && pos2_x-1<=garden.getGardenSize().getWidth()-1 && pos2_y-1>=0 && pos2_y-1<=garden.getGardenSize().getHeight()-1){
							for(int i=0; i<nb2; i++){	
								garden.addObject(new Dimension(pos2_x-1, pos2_y-1), GardenObject.BURIED_CARROT);
							}
						}
						
						break;
					}
					
				//if the first character of the line is a 'R' (rock) ...
				case 'R':
					// we split the line
					String[] wordR = line.split(" ");
					
					if (wordR.length != 2){
						LOGGER.debug("WARNING: line does not follow conventions");
						break;
					}
					
					else{
						// we get the position of the carrot
						String[] pos3 = wordR[1].split("-");
						int pos3_x = Integer.parseInt(pos3[0]);
						int pos3_y = Integer.parseInt(pos3[1]);
						
						//... we add a rock at the indicated position in the garden
						if(pos3_x-1>=0 && pos3_x-1<=garden.getGardenSize().getWidth()-1 && pos3_y-1>=0 && pos3_y-1<=garden.getGardenSize().getHeight()-1)
							garden.addObject(new Dimension(pos3_x-1, pos3_y-1), GardenObject.ROCK);
	
						break;
					}
					
				case ' ':
				case '#':
				case '\0':
					// if it's one of those char, we just skip the line
					break;
					
				default:
					LOGGER.debug("WARNING: invalid line in the garden file");
					break;
			}
		}
		
		LOGGER.info("Garden read");

		//we catch FileNotFoundException + error message
		}
		catch(FileNotFoundException ex) {
			LOGGER.warn("File not found, can't generate garden");
		}
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
}
