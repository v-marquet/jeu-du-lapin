package org.bitbucket.vmarquet.GardenControlers;

import org.bitbucket.vmarquet.Model.*;

import org.apache.log4j.Logger;
import java.util.Random;
import java.awt.Dimension;

public class GardenGeneratorControler {
	
	// Creation of a logger
	private static final Logger LOGGER = Logger.getLogger(GardenGeneratorControler.class);
	
	//garden : defined by height and width
	public GardenMatrix garden;
	private int width, height;
	
	private GameModel model = null;
	
	//Creates a pseudo random garden (random height, width, elements in the garden)
	//and store it in GardenMatrix garden
	public GardenGeneratorControler() {
		
		//random value, only once in the program
		Random rand = new Random();

		//height and width, random sizes between 5 and 10
		width  = rand.nextInt(5)+5;
		height = rand.nextInt(5)+5;
		
		// Display the random values
		LOGGER.debug("Width = " + width + ", Height = " + height);
		
		//initialization of the garden with random dimensions
		garden = new GardenMatrix(new Dimension(width, height));
		this.model = GameModel.getInstance();
		this.model.setGardenMatrix(garden);
				
		//for each cells in the garden ...  
		for(int i=0; i<width; i++){
			for(int j=0; j<height; j++){
					
				//btwOneAndTwelve takes a random value between 1 and 12
				int btwOneAndTwelve = rand.nextInt(11)+1;
				LOGGER.debug("btwOneAndTwelve = " + btwOneAndTwelve);
				
				//if btwOneAndTwelve == 1 (approx. 8 % chances) ...
				if(btwOneAndTwelve == 1){
						
					//... we add between 0 and 5 carrots ...
					int btwZeroAndFive = rand.nextInt(5);
					LOGGER.debug("Adding carrots: btwZeroAndFive = " + btwZeroAndFive);
					
					for(int k=0; k<btwZeroAndFive; k++){
							
						garden.addObject(new Dimension(i,j), GardenObject.CARROT);
					}
							
					//... and we add between 0 and 5 buried carrots
					btwZeroAndFive = rand.nextInt(5);
					LOGGER.debug("Adding buried carrots: btwZeroAndFive = " + btwZeroAndFive);

					for(int k=0; k<btwZeroAndFive; k++){
							
						garden.addObject(new Dimension(i,j), GardenObject.BURIED_CARROT);
					}
							
				}
						
				//if btwOneAndSix == 2 (approx. 8 % chances) ...
				if(btwOneAndTwelve == 2){
							
					//... we add a rock
					garden.addObject(new Dimension(i,j), GardenObject.ROCK);
				}
						
			}
					
		}
				
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
}
