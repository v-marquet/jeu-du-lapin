package org.bitbucket.vmarquet.GardenControlers;

import org.bitbucket.vmarquet.Model.*;

import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.awt.Dimension;
import java.io.*;

public class RabbitReaderControler {

	private static final Logger LOGGER = Logger.getLogger(RabbitReaderControler.class);

	//make player
	private Player player;

	private GameModel model;

	//orientation of the rabbit
	char orientation;

	public RabbitReaderControler(String filename) {

		this.model = GameModel.getInstance();

		try{
			// Reading the file rabbits.txt ...
			LOGGER.debug("RabbitReaderController: Opening file " + filename + " for reading");
			Scanner scanner = new Scanner(new File(filename));
			LOGGER.info("Reading the rabbits...");

			//... line by line 
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				LOGGER.debug(line);

				//if the first character of the line is a 'L' (rabbit) ...
				if(line.charAt(0) == 'L'){

					// we split the line to parse all data
					String[] words = line.split(" ");
					if (words.length != 5) {
						LOGGER.debug("WARNING: invalid line in the rabbits file");
					}

					// we get the initial position
					String[] position = words[1].split("-");
					int pos_x = Integer.parseInt(position[0]);
					int pos_y = Integer.parseInt(position[1]);
					// we get the initial orientation
					char orientation = words[2].charAt(0);
					// we get the list of movements
					String movements = words[3];
					// we get the name
					String name = words[4];

					player = new Player();

					//... we initialize its position
					player.setPositionInMatrix(new Dimension(pos_x-1,pos_y-1));  // ATTENTION: le modèle commence à 0 donc mettre -1

					//... we initialize its orientation
					player.setOrientation(orientation);
					// TODO: d'après l'UML, on devrait utiliser une enum pour l'orientation

					//... we initialize its movements
					int cursor = 0;

					do {

						switch(movements.charAt(cursor)) {


						//If we have to move forward ...
						case 'A':

							if(orientation == 'N') player.addMovement(Movements.MOVE_UP);
							if(orientation == 'E') player.addMovement(Movements.MOVE_RIGHT);
							if(orientation == 'S') player.addMovement(Movements.MOVE_DOWN);
							if(orientation == 'O') player.addMovement(Movements.MOVE_LEFT);
							break;

							//If we have to turn left ...
						case 'G':


							switch(orientation){
							case 'N':
								orientation = 'O';
								break;
							case 'E':
								orientation = 'N';
								break;
							case 'S':
								orientation = 'E';
								break;
							case 'O':
								orientation = 'S';
								break;
							default:
								break;
							}
							//player.setOrientation(orientation);
							player.addMovement(Movements.TURN_LEFT);
							break;

							//If we have to turn right ...
						case 'D':

							switch(orientation){
							case 'N':
								orientation = 'E';
								break;
							case 'E':
								orientation = 'S';
								break;
							case 'S':
								orientation = 'O';
								break;
							case 'O':
								orientation = 'N';
								break;
							default:
								break;
							}

							//player.setOrientation(orientation);
							player.addMovement(Movements.TURN_RIGHT);
							break;

						}

						cursor++;

					} while(cursor < movements.length());

					//we set the name of the player (from the index charNumber+1 to the end of the line)
					player.setName(name);

					//Finally add the player to the players list
					this.model.addPlayer(player);

				}

			}

			LOGGER.info("Rabbits read");

		} catch(FileNotFoundException ex) {
			LOGGER.warn("File not found, can't generate rabbits");
		}
	}

	public RabbitReaderControler(ArrayList<Character>[] playerInput, int width, int height) {
		// Initialization of random generator
		Random rnd = new Random();

		char orientation = 0;
		int pos_x;
		int pos_y;

		this.model = GameModel.getInstance();

		LOGGER.info("Beginning of orientation and position random generation");

		for(int i = 0 ; i < playerInput.length ; i++) {
			
			player = this.model.getPlayerAt(i);
			orientation = player.getOrientation();
			System.out.println("PAf !!");
			for(int j = 0 ; j < playerInput[i].size() ; j++) {
				LOGGER.info("Detect letter " + playerInput[i].get(j));
				switch(playerInput[i].get(j)) { 				

				//If we have to move forward ...
				case 'A':

					if(orientation == 'N') player.addMovement(Movements.MOVE_UP);
					if(orientation == 'E') player.addMovement(Movements.MOVE_RIGHT);
					if(orientation == 'S') player.addMovement(Movements.MOVE_DOWN);
					if(orientation == 'O') player.addMovement(Movements.MOVE_LEFT);
					break;

					//If we have to turn left ...
				case 'G':


					switch(orientation){
					case 'N':
						orientation = 'O';
						break;
					case 'E':
						orientation = 'N';
						break;
					case 'S':
						orientation = 'E';
						break;
					case 'O':
						orientation = 'S';
						break;
					default:
						break;
					}
					//player.setOrientation(orientation);
					player.addMovement(Movements.TURN_LEFT);
					break;

					//If we have to turn right ...
				case 'D':

					switch(orientation){
					case 'N':
						orientation = 'E';
						break;
					case 'E':
						orientation = 'S';
						break;
					case 'S':
						orientation = 'O';
						break;
					case 'O':
						orientation = 'N';
						break;
					default:
						break;
					}

					//player.setOrientation(orientation);
					player.addMovement(Movements.TURN_RIGHT);
					break;
				}
			}
			
			//we set the name of the player (from the index charNumber+1 to the end of the line)
			player.setName("Player" + (i+1));

			//Finally add the player to the players list
			//this.model.addPlayer(player);
		}
	}
}
