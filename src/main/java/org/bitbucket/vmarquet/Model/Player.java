package org.bitbucket.vmarquet.Model;

import java.awt.Dimension;
import java.util.ArrayList;

public class Player {
	
	// Name of the player
	private String name;
	
	// Score of the player
	private int score;
	
	// Orientation of the rabbit (N, S, E, O)
	private char orientation;
	
	// Number of carrots the player have got
	private int carrots;
	
	// List of the movements of the player
	private ArrayList<Movements> moves;
	
	// Position in the GardenPanel in PIXELS
	private Dimension position;
	
	// Last position in the matrix
	// During a move, the rabbit is between two cells
	private Dimension last_position_in_matrix;

	// Constructor of the player
	public Player() {
		this.score = 0;
		this.carrots = 0;
		this.moves = new ArrayList<Movements>();
		// TODO: supprimer les lignes suivantes une fois que les Player seront initialisés correctement
		this.position = new Dimension(0,0); // default value, to prevent from NullPointerException
		this.last_position_in_matrix = new Dimension(0,0); // default value, to prevent from NullPointerException
	}
	
	
	/*
	 * Setters of the Player
	 */
	
	// Method to set the Player's name
	public void setName(String name) {
		this.name = name;
	}
	
	// Method to set the Player's position
	public void setPositionInPixels(Dimension position) {
		this.position = position;
	}
	public void setPositionInMatrix(Dimension position) {
		this.last_position_in_matrix = position;
	}
	// Method to get the Player's position
	public Dimension getPositionInMatrix() {
		return this.last_position_in_matrix;
	}
	public Dimension getPositionInPixels() {
		return this.position;
	}
	
	// Method to set the Player's orientation
	public void setOrientation(char orientation) {
		this.orientation = orientation;
		// TODO: we should check if the lettre is valid
		// or better: use an enum
	}
	public void turnRight() {
		switch(this.orientation) {
			case 'N':
				this.orientation = 'E';
				break;
			case 'E':
				this.orientation = 'S';
				break;
			case 'S':
				this.orientation = 'O';
				break;
			case 'O':
				this.orientation = 'N';
				break;
			default:
				break;
		}
	}
	public void turnLeft() {
		switch(this.orientation) {
			case 'N':
				this.orientation = 'O';
				break;
			case 'O':
				this.orientation = 'S';
				break;
			case 'S':
				this.orientation = 'E';
				break;
			case 'E':
				this.orientation = 'N';
				break;
			default:
				break;
		}
	}

	// Method to set the number of carrots of the player
	public void setCarrots(int carrots){
		this.carrots = carrots;
	}
	public void addCarrot(){
		this.carrots++;
	}
	
	/*
	 * Getters of the Player
	 */
	
	// Method to get the Player's name
	public String getName() {
		return this.name;
	}
	
	// Method to get the Player's orientation
	public char getOrientation() {
		return this.orientation;
	}
	
	// Method to get the next move of the player in the list
	public Movements getNextMove() {  // to get the next move to do during the round
		if ( moves.isEmpty() )
			return null;
		else
			return moves.get(0);  // returns the element at index 0
	}
	
	// Method to get the number of carrots of the player
	public int getCarrots(){
		return this.carrots;
	}
	

	
	// Method to get the Player's score
	public int getScore(){
		return this.score;
	}
	
	
	/*
	 * Other methods
	 */
	
	// Method addToPosition
	public void addToPositionInPixels(Dimension move) {
		this.position.width += move.getWidth();
		this.position.height += move.getHeight();
	}
	public void addToPositionInMatrix(Dimension move) {
		this.last_position_in_matrix.width += move.getWidth();
		this.last_position_in_matrix.height += move.getHeight();
	}
	
	// Method to add a movement in the player's movements list
	public void addMovement(Movements movement) {
		
		moves.add(movement);	
	}
	
	// Method to pop the first move from the palyer's movements list
	public void popNextMove() {  // suppress first move in the list, because it's done
		if ( !moves.isEmpty() )  // if moves list is not already empty
			moves.remove(0);     // removes element at index 0
		return;
	}
	
	public static void main(String[] args) {
	
	return;
	}
}
