package org.bitbucket.vmarquet.Model;

import java.awt.Dimension;
import java.util.ArrayList;
import org.bitbucket.vmarquet.Model.GardenMatrix;

public class GardenMatrix
{

	// Declaration of the elements of the class
	private ArrayList<GardenObject>[][] matrix;
	private int gardenWidth;
	private int gardenHeight;

	@SuppressWarnings("unchecked")
	public GardenMatrix(Dimension dimension) {

		// Here we get the dimensions of the garden
		gardenWidth = (int)dimension.getWidth();
		gardenHeight = (int)dimension.getHeight();

		// Creation of the ArrayList of the garden.
		// http://www.java-forums.org/new-java/28866-2d-array-arraylist-technically-3d-array.html
		matrix = new ArrayList[gardenWidth][gardenHeight];

		// Initialization of the ArrayList.
		for (int j=0 ; j < gardenWidth ; j++) {
			for (int i=0 ; i < gardenHeight ; i++) {
				matrix[j][i] = new ArrayList<GardenObject>();
			}
		}

		System.out.println("Initialisation de GardenMatrix: taille " + gardenWidth + " " + gardenHeight);
	}

	// Method to get the list of elements at a given position in the matrix
	public ArrayList<GardenObject> getObjectsAt(Dimension dimension) {
		int i = (int)dimension.getHeight();
		int j = (int)dimension.getWidth();
		return matrix[j][i];
	}

	// Method to remove a carrot in the matrix
	public void removeCarrot(Dimension dimension) {

		// Split the Dimension into ints
		int i = (int)dimension.getHeight();
		int j = (int)dimension.getWidth();		

		// Remove the carrot in the matrix
		matrix[j][i].remove(GardenObject.CARROT);

	}

	// Method to get the size of the garden
	public Dimension getGardenSize() {

		// Return the dimension object
		return new Dimension(gardenWidth, gardenHeight);

	}

	// Method to add an object in the garden, in a given position
	public void addObject(Dimension dimension, GardenObject object) {

		// Split the Dimension into ints
		int i = (int)dimension.getHeight();
		int j = (int)dimension.getWidth();

		// Add the object into the garden
		matrix[j][i].add(object);

	}

	// Method to remove an object in the garden, in a given position
	public void removeObject(Dimension dimension, GardenObject object) {

		// Split the Dimension into ints
		int i = (int)dimension.getHeight();
		int j = (int)dimension.getWidth();

		// Add the object into the garden
		matrix[j][i].remove(object);

	}


	public static void main(String[] args) {

		return;
	}

}
