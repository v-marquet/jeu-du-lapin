package org.bitbucket.vmarquet.Model;

// enum containing all possible moves

public enum Movements
{
	// depends on where the character is heading:
	GO_FORWARD,
	TURN_RIGHT,
	TURN_LEFT,

	// does not depends on where the character is heading:
	MOVE_UP,
	MOVE_DOWN,
	MOVE_RIGHT,
	MOVE_LEFT;
}
