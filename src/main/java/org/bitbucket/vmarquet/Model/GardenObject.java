package org.bitbucket.vmarquet.Model;

public enum GardenObject {

	// List of the elements which can appear in the garden
	BURIED_CARROT(0, "Buried Carrot"),
	CARROT(1, "Carrot"),
	ROCK(2, "Rock");
	
    // ID of the object
    private int objectID;
	
	// Name of the object
    private String objectName;
	
    // Constructor of the object
    GardenObject(int objectID, String objectName) {
        this.objectID = objectID;
        this.objectName = objectName;
    }

    // Getter of the ID of the object
    public int getObjectID() {
        return objectID;
    }
    
    // Getter of the name of the object
    public String getObjectName() {
        return objectName;
    }

}