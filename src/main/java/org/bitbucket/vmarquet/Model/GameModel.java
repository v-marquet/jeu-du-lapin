package org.bitbucket.vmarquet.Model;

import org.apache.log4j.Logger;
import org.bitbucket.vmarquet.GardenControlers.*;
import java.awt.Dimension;
import java.util.ArrayList;

public class GameModel {
	
	// Creation of a logger
	private static final Logger LOGGER = Logger.getLogger(GameModel.class);
	
	// Declaration of the elements of the class
	private static GameModel INSTANCE = null;
	private ArrayList<Player> players;
	private GardenMatrix garden;
	private String filename;
	
	// Constructor of the GameModel
	private GameModel() {

		// Creation of the players list
		players = new ArrayList<Player>();

	}
	
	// Method to set a garden
	public void setGardenMatrix(GardenMatrix matrix){
		this.garden = matrix;
		
	}
	
	// Method to test whether the garden has been set or not
	public boolean isGardenSet() {
		if (garden == null)
			return false;
		else
			return true;
	}
	
	// Add 'synchronized' if threads problems
	public static GameModel getInstance() {
		if (INSTANCE == null)
			INSTANCE = new GameModel();
		return INSTANCE;
	}
	
	// Method to set a filename
	public void setFilename(String filename){
		
		this.filename = filename;
	}
	
	// Method to get the dimensions of the garden
	public Dimension getGardenSize() {
			
			return garden.getGardenSize();
	}
	
	// Method to set (= create) n players
	public void setNumberOfPlayers(int nbPlayer) {
		
		// we suppress other players
		removePlayers();
		
		// Declaration of a player
		Player player;
		
		// Add nbPlayer players
		for(int i = 0 ; i< nbPlayer ; i++) {
			
			player = new Player();
			players.add(player);
		}
	}
	
	// Method to the the number of players 
	public int getNumberOfPlayers() {
		return players.size();
	}
	
	// Method to get the objects 
	public ArrayList<GardenObject> getObjectsAt(Dimension dimension) {
		return this.garden.getObjectsAt(dimension);
	}

	// Method to remove a carrot in the matrix
	public void removeCarrot(Dimension dimension) {
		
		// Call the method in GardenMatrix
		garden.removeCarrot(dimension);
	}
	
	// Method to get the nth player in the List
	public Player getPlayerAt(int position) {
		
		// Get the player
		return players.get(position);
	}
	
	// Method to generate a garden from a given file
	public void GenerateGardenFromFile (GardenMatrix garden, String filename){
		
		new GardenReaderControler(filename);
	}
	
	// Method to generate the rabbits from a given file
	public void GenerateRabbitsFromFile(ArrayList<Player> players, String filename){
		
		new RabbitReaderControler(filename);
	}
	
	// Method to generate a garden randomly
	public void GenerateGardenRandomly(GardenMatrix garden, String filename){
			
		new GardenGeneratorControler();
	}
	
	// Method to clear the Model between two rounds
	public void clearNewRound() {
		
		// Delete the Matrix
		garden = null;
		
		// Reset the number of carrots of each player to 0		
		for (int i = 0 ; i < players.size() ; i++) {
			
			players.get(i).setCarrots(0);
		}
		
		// Call the garbage collector to remove the Matrix
		System.gc();
		
		// Information message
		LOGGER.info("Matrix cleared");
		
	}
	
	// Method to clear a garden
	public void clearGarden() {
	
		// Delete the Matrix
		garden = null;
	}
	
	// to create a new player
	public void addPlayer(Player player) {
		this.players.add(player);
	}
	
	// to remove all players from the list
	public void removePlayers() {
		this.players.clear();
	}
	
	// Method to get the matrix
	public GardenMatrix getMatrix() {
		return this.garden;
	}
}
