package org.bitbucket.vmarquet.Controler;

import java.awt.Dimension;
import java.util.ArrayList;
import org.apache.log4j.Logger;

import org.bitbucket.vmarquet.Menu;
import org.bitbucket.vmarquet.GUI.GardenPanel;
import org.bitbucket.vmarquet.Model.*;
import org.bitbucket.vmarquet.GUI.Game;


public class GameplayControler implements Runnable
{
	private GameModel model;
	private GardenPanel panel;
	private Menu menu;
	private static final Logger LOGGER = Logger.getLogger(GameplayControler.class);
	
	private final float FPS = 30.0f;  // framerate used by threads: 30 FPS
	private final int move_duration = 2;  // duration of one move, in seconds
	private int move_frame_max;  // number of frames to display one move (move_frame_max = FPS * move_duration)
	private int move_frame_counter = 0;  // to know where we are in the move (10th frame ? 15th frame ? ...). used to make animations
	// IMPORTANT: when move_frame_counter reaches move_frame_max, the move is over
	// the first element of each player's move list is popped
	private float move_achievement;  // pourcentage d'achèvement ajouté à chaque tour de boucle (entre 0 et 1)
	
	// fonction threadée, le coeur du jeu, là où se font les calculs de déplacements
	public void run() {
		try {
			float timeStep = 1/FPS;  // 30 FPS
			int msSleep = Math.round(1000*timeStep); // timeStep in milliseconds
			boolean endOfRound = false;
			
			// we compute the number of frames used to display each move
			this.move_frame_max = Math.round(FPS*move_duration);
			// 30 FPS * 2 secondes => 60 images par tour de boucle
			
			// on calcule le pourcentage d'achèvement ajouté à chaque tour de boucle
			this.move_achievement = 1/(float)move_frame_max;
			
			// we set each player departure position
			int margin_x = GardenPanel.margin_x;
			int margin_y = GardenPanel.margin_y;
			int tile_x = GardenPanel.tile_x;
			int tile_y = GardenPanel.tile_y;
			
			int number_of_players = model.getNumberOfPlayers();
			for (int i=0; i<number_of_players; i++) {
				Player player = model.getPlayerAt(i);
				Dimension pos_mat = player.getPositionInMatrix();
				//LOGGER.debug("POSITION INITIALE RABBIT " + i + " : " + pos_mat.get + " " + pos_y);
				int pos_x = margin_x + (int)pos_mat.getWidth()*tile_x;
				int pos_y = margin_y + (int)pos_mat.getHeight()*tile_y;
				player.setPositionInPixels(new Dimension(pos_x, pos_y));
				LOGGER.debug("POSITION INITIALE RABBIT " + i + " : " + pos_x + " " + pos_y);
			}
			
			// TODO: suite à des problèmes de précision d'affichage car les coordonnées sont stockées en entiers,
			// TODO: il faudrait utiliser des floats partout dans les calculs de positions en pixels
			// je résout le problème temporairement de manière moche avec le tableau suivant:
			float[] position_float_x = new float[number_of_players];
			float[] position_float_y = new float[number_of_players];
			for (int i=0; i<number_of_players; i++) {
				Player player = model.getPlayerAt(i);
				position_float_x[i] = (float)player.getPositionInPixels().getWidth();
				position_float_y[i] = (float)player.getPositionInPixels().getHeight();
			}
			
			while( !endOfRound ) {

				this.move_frame_counter++;
				
				// TODO
				// ici, il faudrait tester si le GardenPanel a été redimensionné par l'utilisateur
				// et si oui, remettre à jour les positions des joueurs AVANT de faire les calculs
				
				// when move_frame_counter reaches move_frame_max, it's time for a new move
				if(move_frame_counter==move_frame_max) {
					move_frame_counter = 0; // we reset move counter
					
					// the first move in a player's move list is the current move, 
					// so when this move is done, we update player position,
					// and we remove this move from the list
					for (int i=0; i<number_of_players; i++) {
						Player player = model.getPlayerAt(i);
						Movements movement = player.getNextMove();
						Dimension last_pos = player.getPositionInMatrix();
						
						// update last_integer_position
						// (le fait de le remettre pile sur une case de la matrice
						// permet d'éviter d'accumuler  un écart crée à chaque tour)
						if (movement != null) {
							switch(movement) {
								case MOVE_UP:
									if (checkIsInGarden(last_pos, new Dimension(0,-1)) == true) {
										player.addToPositionInMatrix(new Dimension(0,-1));
									}
									break;
								case MOVE_DOWN:
									if (checkIsInGarden(last_pos, new Dimension(0,1)) == true) {
										player.addToPositionInMatrix(new Dimension(0,1));
									}
									break;
								case MOVE_RIGHT:
									if (checkIsInGarden(last_pos, new Dimension(1,0)) == true) {
										player.addToPositionInMatrix(new Dimension(1,0));
									}
									break;
								case MOVE_LEFT:
									if (checkIsInGarden(last_pos, new Dimension(-1,0)) == true) {
										player.addToPositionInMatrix(new Dimension(-1,0));
									}
									break;
								case TURN_RIGHT:
									if(move_frame_counter == (move_frame_max/2) )
										player.turnRight();
									break;
								case TURN_LEFT:
									if(move_frame_counter == (move_frame_max/2) )
										player.turnLeft();
									break;
								default:
									break;
							}
						}
						
						// we remove the move from the list
						model.getPlayerAt(i).popNextMove();
						
						// we update the player position
						Dimension pos_matrix = player.getPositionInMatrix();
						int pos_pixel_x = tile_x*(int)pos_matrix.getWidth() + margin_x;
						int pos_pixel_y = tile_y*(int)pos_matrix.getHeight() + margin_y;
						Dimension pos_pixel = new Dimension(pos_pixel_x, pos_pixel_y);
						
						player.setPositionInPixels(pos_pixel);
						
						// if there's a carrot, we eat it
						ArrayList<GardenObject> objects = model.getObjectsAt(pos_matrix);
						for (int k=0; k<objects.size(); k++) {
							GardenObject object = objects.get(k);
							switch (object) {
								case CARROT:
									player.addCarrot();
									model.removeCarrot(pos_matrix);
									break;
								default:
									break;
									
							}
						}
						
						// temporaire, cf todo du dessus
						position_float_x[i] = (float)player.getPositionInPixels().getWidth();
						position_float_y[i] = (float)player.getPositionInPixels().getHeight();
						
					}
				}
				// on continue le déplacement
				else {
					
					int number_of_player_who_finished_to_move = 0;
					
					// on déplace chaque joueur
					for (int i=0; i<number_of_players; i++) {
						Player player = model.getPlayerAt(i);
						Movements movement = player.getNextMove();
						Dimension last_pos = player.getPositionInMatrix();
						
						if (movement != null) {
							switch(movement) {
								case MOVE_UP:
									if (checkIsInGarden(last_pos, new Dimension(0,-1)) == true) {
										
										// temporaire
										position_float_x[i] += 0;
										position_float_y[i] += -tile_y * move_achievement;
										player.setPositionInPixels(new Dimension((int)position_float_x[i], (int)position_float_y[i]));
										
										//player.addToPositionInPixels(new Dimension(0,(int)(-tile_y * move_achievement)));
									}
									break;
								case MOVE_DOWN:
									if (checkIsInGarden(last_pos, new Dimension(0,1)) == true) {
										
										// temporaire
										position_float_x[i] += 0;
										position_float_y[i] += tile_y * move_achievement;
										player.setPositionInPixels(new Dimension((int)position_float_x[i], (int)position_float_y[i]));
										
										//player.addToPositionInPixels(new Dimension(0,(int)( tile_y * move_achievement)));
									}
									break;
								case MOVE_RIGHT:
									if (checkIsInGarden(last_pos, new Dimension(1,0)) == true) {
										
										// temporaire
										position_float_x[i] += tile_x * move_achievement;
										position_float_y[i] += 0;
										player.setPositionInPixels(new Dimension((int)position_float_x[i], (int)position_float_y[i]));
										
										//player.addToPositionInPixels(new Dimension((int)(tile_x * move_achievement), 0));
									}
									break;
								case MOVE_LEFT:
									if (checkIsInGarden(last_pos, new Dimension(-1,0)) == true) {
										
										// temporaire
										position_float_x[i] += - tile_x * move_achievement;
										position_float_y[i] += 0;
										player.setPositionInPixels(new Dimension((int)position_float_x[i], (int)position_float_y[i]));
										
										//player.addToPositionInPixels(new Dimension((int)(- tile_x * move_achievement), 0));
									}
									break;
								case TURN_RIGHT:
									if(move_frame_counter == (move_frame_max/2) )
										player.turnRight();
									break;
								case TURN_LEFT:
									if(move_frame_counter == (move_frame_max/2) )
										player.turnLeft();
									break;
								default:
									break;
							}
						}
						else {
							number_of_player_who_finished_to_move++;
						}
						
						// if all players have done all the moves that were in their move list, we stop the thread
						if (number_of_player_who_finished_to_move == number_of_players) {
							Thread.sleep(3000); // pause de 3 secondes
							LOGGER.info("All players finished their moves, GameplayControler thread destroyed");
							this.menu.switchPanel(Game.PREPARE_NEW_ROUND);
							return;
						}
					}
				}
				this.panel.updateUI();
				Thread.sleep(msSleep); // Synchronize the simulation with real time
			}
		}
		catch(InterruptedException ex) {
			System.err.println(ex.getMessage());
		}
		return;
	}
	
	public GameplayControler(GardenPanel panel, Menu menu) {  // constructeur
		// we get the GameModel (singleton pattern)
		this.model = GameModel.getInstance();
		this.panel = panel;
		this.menu = menu;
	}
	
	// pour vérifier que en partant de la case start, avec le déplacement movement, on soit toujours dans la matrice
	// return true si c'est toujours dans le jardin, faux si ça a dépassé sur le côté
	public boolean checkIsInGarden(Dimension start, Dimension movement) {
		// on récupère la taille du jardin
		int garden_size_x = (int)this.model.getGardenSize().getWidth();
		int garden_size_y = (int)this.model.getGardenSize().getHeight();
		
		// on ajoute le déplacement à la position de départ 
		int pos_x = (int)start.getWidth();
		int pos_y = (int)start.getHeight();
		pos_x += (int)movement.getWidth();
		pos_y += (int)movement.getHeight();
		
		// ATTENTION: ici je suppose que la position commence à 0, pas à 1
		// on teste si l'indice de la case est bien dans la matrice
		boolean rock = false;
		if (pos_x < 0 || pos_x > garden_size_x-1 || pos_y < 0 || pos_y > garden_size_y-1)
			return false;
		// on teste s'il y a un rocher dans la case
		else {
			ArrayList<GardenObject> objects = model.getObjectsAt(new Dimension(pos_x, pos_y));
			for (int i=0; i<objects.size(); i++) {
				GardenObject object = objects.get(i);
				switch(object) {
					case ROCK:
						rock = true;
						break;
					default:
						break;
				}
			}
		}
		if (rock == true)
			return false;
		return true;
	}
	
	public static void main(String[] args) {
      
		return;
	}
   
}
