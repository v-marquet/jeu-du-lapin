package org.bitbucket.vmarquet.GUI;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.bitbucket.vmarquet.GUI.Game;
import org.bitbucket.vmarquet.Controler.GameplayControler;
import org.bitbucket.vmarquet.Model.GameModel;
import org.bitbucket.vmarquet.Model.GardenObject;
import org.bitbucket.vmarquet.Model.Movements;
import org.bitbucket.vmarquet.Model.Player;

import java.awt.Dimension;

@SuppressWarnings("serial")
public class GardenPanel extends JPanel
{
	// TODO
	// for better performances, Image objects should be created only once and then stored
	// (for now, we load images from files each time we update the display)
	
	private final static String res = "src/main/resources/";
	
	private final int nb_tiles_x;  // number of tiles on horizontal axis
	private final int nb_tiles_y;  // humber of tiles on vertical axis
	
	private GameModel model;
	private GameplayControler controler;
	
	// size of each tile (updated in paintGarden() )
	// TODO: set tile_x, tile_y, margin_x and margin_y to private and create a getter (they are needeed in GameplayControler)
	public static int tile_x;
	public static int tile_y;
	// margin on the side of the garden, for a prettier display
	public static int margin_x;
	public static int margin_y;
	
	private int number_of_grass_images = 2;
	private Image[] grass_images;
	private int[][] garden_tiles_list;
	
	// pictures loaded in loadPictures()
	private final String file_rabbit_up = "pictures/rabbit_sprite_up.png";
	private final String file_rabbit_down = "pictures/rabbit_sprite_down.png";
	private final String file_rabbit_left = "pictures/rabbit_sprite_left.png";
	private final String file_rabbit_right = "pictures/rabbit_sprite_right.png";
	// corresponding Image
	private Image rabbit_up;
	private Image rabbit_down;
	private Image rabbit_left;
	private Image rabbit_right;
	
	// to know if it's a preview of the garden
	private boolean preview;

	/**
	 * @param size
	 */
	public GardenPanel(Dimension size, boolean preview) {  // constructeur
		// we get the GameModel (singleton pattern)
		this.model = GameModel.getInstance();
		
		this.preview = preview;
		
		// we load the pictures
		loadPictures();
		
		this.nb_tiles_x = (int)size.getWidth();
		this.nb_tiles_y = (int)size.getHeight();

		// we load a random grass image, if it's always the same, it's too obvious and not realist
		garden_tiles_list = new int[nb_tiles_x][nb_tiles_y];
		for (int i=0; i<nb_tiles_x; i++) {
			for (int j=0; j<nb_tiles_y; j++) {
				Random rand = new Random();
				garden_tiles_list[i][j] = Math.abs(rand.nextInt()) % number_of_grass_images;  // modulo number of images available
			}
		}
		grass_images = new Image[number_of_grass_images];
		for (int i=0; i<number_of_grass_images; i++) {
			try {
				String image_name = res + "pictures/grass" + Integer.toString(i) + ".png";
				grass_images[i] = ImageIO.read(new File(image_name));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public int getNb_tiles_x(){
		return this.nb_tiles_x;
	}
	public int getNb_tiles_y(){
		return this.nb_tiles_y;
	}

	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		paintGarden(g);  // to draw the grass
		paintObjects(g);  // to draw the objects
		if (preview == true)
			computePositionInPixels();
		paintRabbits(g);  // to draw the rabbits
	}
	
	public void paintGarden(Graphics g) {
		// on caste l'objet Graphics en Graphics2D car plus de fonctionnalit��s
		Graphics2D g2d = (Graphics2D) g;

		// to obtain the size of the panel
		int w = this.getWidth();
		int h = this.getHeight();

		// we clear the whole panel
		Color backgroundColor = Color.decode("#61A627"); 
		// Color backgroundColor = new Color(127,203,32);
		g2d.setBackground(backgroundColor);
		g2d.clearRect(0,0,w,h);

		// if we want to paint with a gradient
		// GradientPaint gp = new GradientPaint(0, 0, Color.RED, 0, this.getHeight(), Color.cyan, true);
		// GradientPaint gp = new GradientPaint(0, 0, Color.decode("#4b8322"), 0, this.getHeight()/2, Color.decode("#82cf1f"), true);
		// g2d.setPaint(gp);
		// g2d.fillRect(0, 0, this.getWidth(), this.getHeight()); 

		computeTileAndMarginSize();
		
		// we draw the rectangles
		for (int i=0; i<nb_tiles_x; i++) {
			for (int j=0; j<nb_tiles_y; j++) {
				if ( (i+j)%2 == 0)   // dark grass
					g2d.setColor(Color.BLUE);
				else {  // light grass
					g2d.setColor(Color.RED);
					// try {
						Image img = this.grass_images[ this.garden_tiles_list[i][j] ];
						g2d.drawImage(img, margin_x + i*tile_x, margin_y + j*tile_y, tile_x, tile_y, this);
					// } catch (IOException e) {
					// 	g2d.fillRect(margin_x + i*tile_x, margin_y + j*tile_y, tile_x, tile_y);
					// 	e.printStackTrace();
					// } 
				}
				
				// g2d.drawRect(margin_x + i*tile_x, margin_y + j*tile_y, tile_x, tile_y);
			}
		}

		// we draw the trees
		float ratio_source = (float)tile_x/110;
		ratio_source *= (float)0.75;
		//System.out.println(ratio_source);
		try {
			String image_name = res + "pictures/buisson1.png";
			Image buisson = ImageIO.read(new File(image_name));
			g2d.drawImage(buisson, (int)(this.getWidth()-307*ratio_source), (int)(this.getHeight()-210*ratio_source), (int)(307*ratio_source), (int)(210*ratio_source), this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	// to draw the objects, such as rocks and carrots
	public void paintObjects(Graphics g) {
		// on caste l'objet Graphics en Graphics2D car plus de fonctionnalités
		Graphics2D g2d = (Graphics2D) g;
		
		try {
			// we load the sprites
			String rock_pic = res + "pictures/rock.png";
			Image rock = ImageIO.read(new File(rock_pic));
			String carrot_pic = res + "pictures/carrot.png";
			Image carrot = ImageIO.read(new File(carrot_pic));
					
			// for each position in the garden, we get the objects and we draw them
			for (int i=0; i<nb_tiles_x; i++) {
				for (int j=0; j<nb_tiles_y; j++) {
					// we get the list of objects at that position
					ArrayList<GardenObject> objects = model.getObjectsAt(new Dimension(i, j));
					// for each item in the list, we draw it
					int number_of_carrots = 0;
					for (int k=0; k<objects.size(); k++) {
						switch(objects.get(k)) {
							case ROCK:
								g2d.drawImage(rock, margin_x + i*tile_x, margin_y + j*tile_y, tile_x, tile_y, this);
								break;
							case CARROT:
								g2d.drawImage(carrot, margin_x + i*tile_x, margin_y + j*tile_y, tile_x, tile_y, this);
								number_of_carrots++;
								break;
							default:
								break;
						}
					}
					// we draw the number of carrots
					if (number_of_carrots > 0) {
						g2d.drawString(Integer.toString(number_of_carrots), margin_x + i*tile_x +10, margin_y + j*tile_y +10);
					}
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// to draw the rabbits on the garden
	public void paintRabbits(Graphics g) {
		// on caste l'objet Graphics en Graphics2D car plus de fonctionnalit��s
		Graphics2D g2d = (Graphics2D) g;

		try {
			// image pour tester, apr��s il faudra rajouter les animations, etc...
			String image_name = res + "pictures/rabbit_test.png";
			Image lapin_test = ImageIO.read(new File(image_name));
			
			// pour chaque joueur, on l'affiche dans le GardenPanel
			for (int i=0; i<model.getNumberOfPlayers(); i++) {
				Player player = model.getPlayerAt(i);
				int x = (int)player.getPositionInPixels().getWidth();
				int y = (int)player.getPositionInPixels().getHeight();
				Image image;
				switch( player.getOrientation() ) {
					case 'N':
						image = rabbit_up;
						break;
					case 'O':
						image = rabbit_left;
						break;
					case 'S':
						image = rabbit_down;
						break;
					case 'E':
						image = rabbit_right;
						break;
					default:
						image = rabbit_down;
						break;
				}
				g2d.drawImage(image, x, y, tile_x, tile_y, this);
				g2d.drawString("< Player " + (i+1) + " (" + player.getCarrots() + ") >", x, y-5);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void computePositionInPixels() {
		for (int i=0; i<model.getNumberOfPlayers(); i++) {
			Player player = model.getPlayerAt(i);
			
			Dimension pos_matrix = player.getPositionInMatrix();
			int pos_pixel_x = tile_x*(int)pos_matrix.getWidth() + this.margin_x;
			int pos_pixel_y = tile_y*(int)pos_matrix.getHeight() + this.margin_y;
			
			player.setPositionInPixels(new Dimension(pos_pixel_x, pos_pixel_y));
		}
	}
	
	// we convert the pictures to Image objects
	private void loadPictures() {
		try {
			this.rabbit_up = ImageIO.read(new File(res + this.file_rabbit_up));
			this.rabbit_down = ImageIO.read(new File(res + this.file_rabbit_down));
			this.rabbit_left = ImageIO.read(new File(res + this.file_rabbit_left));
			this.rabbit_right = ImageIO.read(new File(res + this.file_rabbit_right));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void computeTileAndMarginSize() {
		// to obtain the size of the panel
		int w = this.getWidth();
		int h = this.getHeight();
		
		// we remove 20 on each side, for esthetic purposes
		margin_x = 10;
		margin_y = 10;
		w -= 2*margin_x;
		h -= 2*margin_y;

		// we compute the size of each tile of the garden (ratio of images: 110/135 = 0,814814815)
		tile_x = w/nb_tiles_x;
		tile_y = h/nb_tiles_y;
		float ratio = (float)tile_x / (float)tile_y;
		// pour le moment, les tiles remplissent la zone du jardin, mais ne sont pas au bon ratio
		// donc on change le ratio 
		if ( ratio > 0.81 ) {  // les tiles sont trop larges
			tile_x /= ( ratio / 0.81 );
			margin_x += ( this.getWidth() - margin_x - tile_x*nb_tiles_x ) / 2;
		}
		if ( ratio < 0.81 ) {  // les tiles sont trop hautes
			tile_y /= ( 0.81 / ratio );
			margin_y += ( this.getHeight() - margin_y - tile_y*nb_tiles_y ) / 2;
		}
	}
	
	public static void main(String[] args) {
		JFrame fen = new JFrame();
		Dimension size = new Dimension(6,4);
		GardenPanel garden = new GardenPanel(size, true);
		fen.add(garden);
		fen.setSize(600,400);
		fen.setVisible(true);

		return;
		// SwingUtilities.invokeLater(new Runnable() {
		// @Override
		// public void run() {
		// 	LinesExample ex = new LinesExample();
		// 	ex.setVisible(true);
		// 	}
		// });
	}

}

