package org.bitbucket.vmarquet.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.bitbucket.vmarquet.Menu;
import org.bitbucket.vmarquet.GardenControlers.GardenGeneratorControler;
import org.bitbucket.vmarquet.GardenControlers.GardenReaderControler;
import org.bitbucket.vmarquet.GardenControlers.RabbitReaderControler;
import org.bitbucket.vmarquet.Model.GameModel;
import org.bitbucket.vmarquet.Model.GardenObject;
import org.bitbucket.vmarquet.Model.Player;
import org.bitbucket.vmarquet.GUI.Game;
import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class NewRoundPanel extends JPanel implements KeyListener {
	private static final Logger LOGGER = Logger.getLogger(NewRoundPanel.class);
	private JPanel choicePanel;
	private JPanel gardenPreview;
	private JPanel playerInputPanel;
	private JPanel topPanel;
	private JPanel bottomPanel;
	private JPanel footerPanel;
	private JPanel loadRabbitPanel;
	private JPanel inputPanel;
	private JFileChooser file;
	private JButton loadGarden;
	private JButton loadRabbit;
	private JButton playButton;
	private JButton randomGarden;
	private JTextField[] inputWay;
	private JLabel[] playerField;
	private JLabel instructionsLabel;
	private JLabel errorWhenPressingPlay;
	private GardenPanel previewPanel;
	private GameModel model;
	private int playerNumber;
	private boolean jTextFieldFlag = false;
	private boolean isRandomGarden = false; // flag de chargement de terrain aléatoirement
	private boolean isLoadedGarden = false; // flag de chargement de terrain via fichier
	private String fileName;
	private String[] fieldInput;
	private GardenReaderControler gardenLoad;
	private GardenGeneratorControler gardenRandom;
	// playerHand Contiendra l'ensemble des directions de chaque joueur
	private ArrayList<Character> playerHand[];
	// Instructions string which will put in instructionsLabel 
	private String instructionsStr = "<html>" +
			"To move your rabbit, you can either load his moves from a file <br><center>or</center>" +
			" Put your rabbit moves (A, D, or G letter) in (the) text(s) box<br>" +
			"A is to go forward<br>" +
			"G is to turn left<br>" +
			"D is to turn right<br>" +
			" Enjoy !</html>";

	private String errorWhenPressingPlayMessage = "";

	//boolean pour check si un jardin a ete genere (activation ou non du bouton play)
	private boolean isGardenGenerated = false;
	//boolean pour check si des lapins on ete load (activation ou non du bouton play)
	private boolean isRabbitsGenerated = false;

	/**
	 * @param menu
	 */
	@SuppressWarnings("unchecked")
	public NewRoundPanel(Menu menu) {
		// to enable KeyListener
		this.addKeyListener(this);

		// Number of players initialization
		this.model = GameModel.getInstance();
		this.playerNumber = model.getNumberOfPlayers();
		if(playerNumber == 0)	playerNumber = 1; // Default number of player
		System.out.println("Log : nombre de joueurs = " + playerNumber);

		// Panel
		this.choicePanel = new JPanel(new BorderLayout());
		//this.choicePanel.setBorder(BorderFactory.createTitledBorder("Preparation of next round"));

		this.gardenPreview = new JPanel(new GridLayout(1,1));
		this.gardenPreview.setBorder(BorderFactory.createTitledBorder("Garden Preview"));

		this.topPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		this.topPanel.setBorder(BorderFactory.createTitledBorder("Please select the way to generate the garden"));

		this.bottomPanel = new JPanel(new GridLayout(1,2));
		//this.bottomPanel.setBorder(BorderFactory.createTitledBorder("Please enter the movements of the rabbits"));

		this.footerPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		//this.footerPanel.setBorder(BorderFactory.createTitledBorder("Let's play !"));

		this.playerInputPanel = new JPanel(new GridLayout(4,1));
		this.playerInputPanel.setBorder(BorderFactory.createTitledBorder("Instructions"));

		this.loadRabbitPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		//this.loadRabbitPanel.setBorder(BorderFactory.createTitledBorder("Load Rabbit"));

		this.inputPanel = new JPanel(new GridLayout((playerNumber),2));
		this.inputPanel.setBorder(BorderFactory.createTitledBorder("Or"));

		this.inputPanel = new JPanel(new GridLayout((playerNumber),2));
		this.gardenPreview.setBorder(BorderFactory.createTitledBorder("Preview Display"));

		// Text field		
		this.inputWay = new JTextField[playerNumber];
		this.playerField = new JLabel[playerNumber];
		this.instructionsLabel = new JLabel(instructionsStr);
		this.errorWhenPressingPlay = new JLabel(errorWhenPressingPlayMessage);
		this.errorWhenPressingPlay.setForeground(Color.RED);
		this.playerHand = new ArrayList[playerNumber];
		this.fieldInput = new String[playerNumber];

		// Buttons
		this.setLayout(new BorderLayout());
		this.playButton = new JButton("Play");
		this.loadRabbit = new JButton("Load player(s) rabbit");
		this.loadGarden = new JButton("Load garden");
		this.randomGarden = new JButton("Random Garden");

		// Link buttons with ActionListener
		// Load Garden button
		this.loadGarden.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isRandomGarden = false;
				System.out.println("we are choosing a garden");
				fileChooserGardenReader(choicePanel);
				isLoadedGarden = true;

				updateGardenPreview(); // we draw the preview, if a garden was created
			}
		});

		// Random garden button
		this.randomGarden.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isLoadedGarden = false;
				gardenRandom = new GardenGeneratorControler();
				int width = gardenRandom.getWidth();
				int height = gardenRandom.getHeight();
				randomPlayerPosition(playerHand, width, height);
				System.out.println("Random garden generated!");
				isGardenGenerated = true;
				isRandomGarden = true;
				updateGardenPreview(); // we draw the preview, if a garden was created
			}
		});

		// Load rabbit button
		this.loadRabbit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// if we load a file containing the rabbits, the number of rabbits can be different 
				// from the number the user set in the main menu
				// we must clear the list of players
				GameModel model = GameModel.getInstance();
				model.removePlayers();

				// we call the RabbitReaderGenerator
				fileChooserRabbitReader(choicePanel);
				/*int widthL = gardenLoad.getWidth();
				int heightL = gardenLoad.getHeight();
				randomPlayerPosition(playerHand, widthL, heightL);*/
				// Display rabbit on the preview garden
				updateGardenPreview();

				System.out.println("Rabbit loaded");
			}
		});

		// Play button
		this.playButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(jTextFieldFlag == true) {
					RabbitReaderControler garden;

					// Fill the arrayList with input(s) player(s)
					for(int i = 0 ; i < playerNumber ; i++)
						playerHand[i] = inputFilter(fieldInput[i]);

					//si des lapins etaient deja load on clear les joueurs
					if(isRabbitsGenerated == true){

						model.removePlayers();
					}

					// Update the player input in random or loaded garden controler
					if(isRandomGarden == true)
						garden = new RabbitReaderControler(playerHand, gardenRandom.getWidth(), gardenRandom.getHeight());
					else if(isLoadedGarden == true)
						garden = new RabbitReaderControler(playerHand, gardenLoad.getWidth(), gardenLoad.getHeight());

					isRabbitsGenerated = true;

					// Display rabbit on the preview garden
					updateGardenPreview();
				}
				if(isGardenGenerated == false && isRabbitsGenerated == true)
					errorWhenPressingPlay.setText("Please select a garden (generated from a file or randomly)  ");

				if(isRabbitsGenerated == false && isGardenGenerated == true)
					errorWhenPressingPlay.setText("Please generate rabbits movements (from a file or manualy)  ");

				if(isGardenGenerated == false && isRabbitsGenerated == false)
					errorWhenPressingPlay.setText("Please select a garden (generated from a file or randomly) and generate rabbits movements (from a file or manualy)  ");

				//SI on a charge le garden et les lapins alors on peut cliquer sur play
				if(isGardenGenerated == true && isRabbitsGenerated == true){
					Menu menu = Menu.getInstance();
					menu.switchPanel(Game.IN_GAME);
				}
			}
		});

		// Add at the panel
		this.topPanel.add(loadGarden);
		this.topPanel.add(randomGarden);
		this.choicePanel.add(topPanel, BorderLayout.NORTH);
		this.choicePanel.add(bottomPanel, BorderLayout.CENTER);
		this.choicePanel.add(footerPanel, BorderLayout.SOUTH);
		this.footerPanel.add(errorWhenPressingPlay);
		this.footerPanel.add(playButton);
		this.bottomPanel.add(playerInputPanel, BorderLayout.WEST);
		this.bottomPanel.add(gardenPreview, BorderLayout.EAST);
		this.playerInputPanel.add(instructionsLabel);
		this.playerInputPanel.add(loadRabbitPanel);
		this.playerInputPanel.add(inputPanel);
		this.loadRabbitPanel.add(loadRabbit, BorderLayout.NORTH);

		// Add text(s) field(s)
		for(int i = 0 ; i < playerNumber ; i++) {
			this.playerField[i] = new JLabel("Player " + (i+1));
			this.inputWay[i] = new JTextField();
			this.inputWay[i].addKeyListener(new playerKeyListener());
			this.inputPanel.add(playerField[i]);
			this.inputPanel.add(inputWay[i]);
			this.inputWay[i].setPreferredSize(new Dimension(1, 10));
		}

		// Display choice panel
		this.add(choicePanel);
		
		this.updateUI();
	}

	/**
	 * @brief 
	 * @param panel
	 */
	// Recuperation d'un fichier (a partir d'un JFileChooser) pour charger le jardin
	private void fileChooserGardenReader(JPanel panel) {


		fileName = Paths.get("").toAbsolutePath().toString();
		file = new JFileChooser(new File(fileName));
		file.setCurrentDirectory(new File("./src/main/resources/gardens/"));
		int result = file.showOpenDialog(panel);

		switch (result) {

		//Si on a choisi un fichier
		case JFileChooser.APPROVE_OPTION:
			System.out.println("Approve Open was clicked");



			if(isGardenGenerated == true){

				model.clearGarden();
			}

			//on charge le garden
			fileName = file.getSelectedFile().toString();
			System.out.println("File : " + fileName);
			gardenLoad = new GardenReaderControler(fileName);

			//on met isGardenGenerated a true si le fichier est correct
			if(gardenLoad.garden.getGardenSize().getHeight() == 0 || gardenLoad.garden.getGardenSize().getWidth() == 0){	
				System.out.println("Wrong file");
				gardenLoad = null;
				errorWhenPressingPlay.setText("Please select a correct file  ");
			}
			else{
				System.out.println("The garden file is hmmm correct and dimension are : (" + gardenLoad.garden.getGardenSize().getWidth()  + "; " + gardenLoad.garden.getGardenSize().getHeight() +")");
				isGardenGenerated = true;
			}

			break;

			//si on a canceled, on fait rien
		case JFileChooser.CANCEL_OPTION:
			System.out.println("Cancel or the close-dialog icon was clicked");
			break;

			//erreur
		case JFileChooser.ERROR_OPTION:
			System.out.println("Error");
			break;
		}
	}

	// Recuperation d'un fichier (à partir d'un JFileChooser) pour charger les rabbits
	@SuppressWarnings("unused")
	private void fileChooserRabbitReader(JPanel panel) {


		fileName = Paths.get("").toAbsolutePath().toString();
		file = new JFileChooser(new File(fileName));
		file.setCurrentDirectory(new File("./src/main/resources/rabbits/"));
		int result = file.showOpenDialog(panel);

		switch (result) {

		//Si on a choisi un fichier
		case JFileChooser.APPROVE_OPTION:
			System.out.println("Approve Open was clicked");

			//si des lapins etait deja load on clear les joueurs
			if(isRabbitsGenerated == true){

				model.removePlayers();
			}

			// On charge le garden
			fileName = file.getSelectedFile().toString();
			System.out.println("File : " + fileName);
			RabbitReaderControler garden = new RabbitReaderControler(fileName);

			isRabbitsGenerated = true;

			break;

			//si on a canceled, on fait rien
		case JFileChooser.CANCEL_OPTION:
			System.out.println("Cancel or the close-dialog icon was clicked");
			break;

			//erreur
		case JFileChooser.ERROR_OPTION:
			System.out.println("Error");
			break;
		}
	}

	// Présente pour filtrer les caractères non recevablent à lancer avant l'enrengistrement et lancement du jeu et stocker dans playerHand
	private ArrayList<Character> inputFilter(String str) {
		char c;
		ArrayList<Character> cList = new ArrayList<Character>();

		System.out.println("String size : " + str.length());

		// For each character of the string
		for(int i = 0 ; i < str.length() ; i++) {
			c = str.charAt(i);
			System.out.println("Log : enter " + c + " character et i = " + i);

			// We filter the character value
			if(c == 'A' || c == 'D' || c == 'G') {
				cList.add(new Character(c));
			} else	System.out.println("Log : invalid com");
		}
		System.out.println("List size : " + cList.size());
		return cList;
	}

	private void updateGardenPreview() {

		if (this.model.isGardenSet() == true) {

			if(previewPanel != null){
				this.gardenPreview.remove(previewPanel);
				this.gardenPreview.validate();
			}
			this.previewPanel = new GardenPanel(this.model.getGardenSize(), true);
			this.gardenPreview.add(previewPanel, BorderLayout.EAST);
		}
		this.updateUI();
		return;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame fen = new JFrame();
		fen.setSize(1280,720);
		fen.setResizable(false);
		NewRoundPanel panel = new NewRoundPanel(null);
		fen.add(panel);
		fen.setVisible(true);

		return;
	}

	/**
	 * @brief 
	 *
	 */
	class playerKeyListener implements KeyListener{
		public void keyReleased(KeyEvent event) {
			// Reception de la saisie du joueur X
			for(int i = 0 ; i < playerNumber ; i++) {
				fieldInput[i] = inputWay[i].getText().toUpperCase();
				System.out.println("Log : J" + (i+1) + " = " + fieldInput[i]);
			}
			jTextFieldFlag = true;
		}

		public void keyPressed(KeyEvent event) {
		/*	switch (event.getKeyCode()) {
			case KeyEvent.VK_ENTER:
				@SuppressWarnings("unused")
				RabbitReaderControler garden;

				// Fill the arrayList with input(s) player(s)
				for(int i = 0 ; i < playerNumber ; i++)
					playerHand[i] = inputFilter(fieldInput[i]);

				//si des lapins etaient deja load on clear les joueurs
				if(isRabbitsGenerated == true){

					model.removePlayers();
				}

				// Update the player input in random or loaded garden controler
				if(isRandomGarden == true)
					garden = new RabbitReaderControler(playerHand, gardenRandom.getWidth(), gardenRandom.getHeight());
				else if(isLoadedGarden == true)
					garden = new RabbitReaderControler(playerHand, gardenLoad.getWidth(), gardenLoad.getHeight());

				isRabbitsGenerated = true;

				// Display rabbit on the preview garden
				updateGardenPreview();

				break;
			default:
				break;
			}*/
		}

		public void keyTyped(KeyEvent event) {}   	
	}   

	private void randomPlayerPosition(ArrayList<Character>[] players, int width, int height){
		int pos_x,pos_y;
		char orientation;
		Random rnd = new Random();



		for(int i = 0; i < players.length; i++){
			do {
				pos_y = rnd.nextInt(height) +1;
				pos_x = rnd.nextInt(width) +1;
				LOGGER.info("Radom position give : (" + pos_x + ";" + pos_y + ")");
				LOGGER.info(model.getMatrix().getObjectsAt(new Dimension(pos_x-1, pos_y-1)));
			} while(model.getMatrix().getObjectsAt(new Dimension(pos_x -1, pos_y-1)).contains(GardenObject.ROCK));


			LOGGER.info("Begin of random orientation");
			do {
				orientation = (char) (65 + rnd.nextInt(20));
				if((orientation == 'N') || (orientation == 'E') || (orientation == 'S') || (orientation == 'O'))
					break;
			}while(true);

			Player player = this.model.getPlayerAt(i);

			//... we initialize its position
			player.setPositionInMatrix(new Dimension(pos_x-1,pos_y-1));  // ATTENTION: le modèle commence à 0 donc mettre -1

			//... we initialize its orientation
			player.setOrientation(orientation);
			//this.model.addPlayer(player);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {}
		this.updateUI(); // pour repeindre le panel
	}

	@Override
	public void keyReleased(KeyEvent e) {}
	@Override
	public void keyTyped(KeyEvent e) {}
}
