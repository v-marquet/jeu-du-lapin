package org.bitbucket.vmarquet.GUI;

import javax.swing.*;

import org.bitbucket.vmarquet.Model.GameModel;
import org.bitbucket.vmarquet.Menu;
import org.bitbucket.vmarquet.GUI.Game;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MenuPanel extends JPanel implements KeyListener
{
	private final static String res = "src/main/resources/";
	private final static String background = "pictures/menu.jpg";
	
	private int numberOfPlayer;  // pour stocker le nombre de joueurs
	private final int numberOfPlayerMax = 6;
	private Menu menu; // la classe Menu, pour lui envoyer le signal de changement de panel
	private GameModel model; // le modèle du jeu

	public MenuPanel() {  // constructeur
		this.numberOfPlayer = 1;
		this.model = GameModel.getInstance();
		this.addKeyListener(this); // to enable KeyListener
	}
	public MenuPanel(Menu menu) {  // constructeur
		this.numberOfPlayer = 1;
		this.model = GameModel.getInstance();
		this.addKeyListener(this); // to enable KeyListener
		this.menu = menu;
	}

	// on redéfinit la méthode paint pour afficher les menus sur le Panel
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		// on caste l'objet Graphics en Graphics2D car plus de fonctionnalités
		Graphics2D g2d = (Graphics2D) g;

		// to obtain the size of the panel
		int w = this.getWidth();
		int h = this.getHeight();

		// on affiche l'image de fond
		String filename = this.res.concat(this.background);
		Image im = Toolkit.getDefaultToolkit().getImage(filename);
		g2d.drawImage(im, 0, 0, w, h, this);
		
		// paramètres d'affichage du texte
		g2d.setFont(new Font("TimesRoman", Font.PLAIN, 40)); // dernier arg = taille police
		
		// on affiche le nombre de joueurs
		g2d.setColor(Color.WHITE);
		g2d.drawString("Press ENTER to play", 450, 400);
		g2d.drawString("Number of player: ", 430, 500);
		g2d.drawString(Integer.toString(numberOfPlayer), 800, 500);

		return;
	}

	// pour exécuter cette classe directement, sans le reste du projet
	public static void main(String[] args) {
		JFrame fen = new JFrame();
		fen.setSize(1280,720);
		fen.setResizable(false);
		MenuPanel panel = new MenuPanel();
		fen.add(panel);
		fen.setVisible(true);
		panel.requestFocus();
		
		return;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
			case KeyEvent.VK_ENTER:
				this.model.setNumberOfPlayers(this.numberOfPlayer);
				this.menu.switchPanel(Game.PREPARE_NEW_ROUND);
				break;
			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_DOWN:
				if(numberOfPlayer > 1)
					numberOfPlayer--;
				break;
			case KeyEvent.VK_RIGHT:
			case KeyEvent.VK_UP:
				if(numberOfPlayer < numberOfPlayerMax)
					numberOfPlayer++;
				break;
			default:
				break;
		}
		this.updateUI(); // pour repeindre le panel
	}

	@Override
	public void keyReleased(KeyEvent e) {}
	@Override
	public void keyTyped(KeyEvent e) {}

}