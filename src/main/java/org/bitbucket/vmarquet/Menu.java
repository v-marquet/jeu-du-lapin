package org.bitbucket.vmarquet;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import org.apache.log4j.Logger;

import org.bitbucket.vmarquet.Controler.GameplayControler;
import org.bitbucket.vmarquet.GUI.Game;
import org.bitbucket.vmarquet.GUI.GardenPanel;
import org.bitbucket.vmarquet.GUI.InGameWindow;
import org.bitbucket.vmarquet.GUI.MenuPanel;
import org.bitbucket.vmarquet.GUI.NewRoundPanel;
import org.bitbucket.vmarquet.Model.GameModel;

public class Menu extends JFrame
{
	final static String res = "src/main/resources/";
	private static Menu INSTANCE = null;
	private static final Logger LOGGER = Logger.getLogger(Menu.class);
	
	private MenuPanel menuPanel; // le panel du menu
	private NewRoundPanel newRoundPanel; // le panel entre deux rounds
	private InGameWindow inGamePanel; // le panel du jeu ingame
	private GardenPanel gardenPanel;  // le panel du jardin (prévu pour être une sous partie de InGameWindow, mais plus le temps)
	private GameModel model;  // le modèle du jeu
	private GameplayControler gameplayControler; // le controleur des mouvements des lapins

	private short nbPlayer;
	private final short maxNbPlayer = 6;
	private Game gameMode = Game.MENU;
	private boolean firstExec;

	private Menu() {  // constructeur
		this.model = GameModel.getInstance(); // on instancie le modèle
		this.resetJFrameParameters();  // paramètres de la fenêtre
		firstExec = false;
	}
	public static Menu getInstance() {
		if (INSTANCE == null)
			INSTANCE = new Menu();
		return INSTANCE;
	}
	
	// pour changer le panel à afficher
	public void switchPanel(Game game) {
		switch(game) {
			case MENU:
				this.removeAllPanels();
				this.menuPanel = new MenuPanel(this);
				this.add(menuPanel); // on ajoute le panel dans la fenêtre
				this.pack();
				this.resetJFrameParameters();
				this.menuPanel.requestFocus();
				break;
			case PREPARE_NEW_ROUND:
				this.removeAllPanels();
				this.newRoundPanel = new NewRoundPanel(this);
				this.add(newRoundPanel); // on ajoute le panel dans la fenêtre
				this.pack();
				this.resetJFrameParameters();
				this.newRoundPanel.requestFocus();
				if (firstExec == true) {
					try {
						Thread.sleep(1000);
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					firstExec = true;
				}
				this.newRoundPanel.updateUI();
				break;
			case IN_GAME:
				this.removeAllPanels();
				// debug:
				Dimension dim = model.getGardenSize();
				int w = (int)dim.getWidth();
				int h = (int)dim.getHeight();
				LOGGER.debug("Creating GardenPanel with garden size: w = " + w + "  h = " + h);
				// end debug
				this.gardenPanel = new GardenPanel(model.getGardenSize(), false);
				this.add(gardenPanel);
				this.pack();
				this.resetJFrameParameters();
				this.gardenPanel.requestFocus();
				this.gardenPanel.computeTileAndMarginSize();
				// we start the gameplay controler to move the rabbits
				Thread thread = new Thread(new GameplayControler(gardenPanel, this));
				thread.start();
				break;
			default:
				break;
		}
		
	}
	
	// il faut supprimer le panel de la JFrame avant de pouvoir en ajouter un autre à la place
	private void removeAllPanels() {
		if (menuPanel != null)
			this.remove(menuPanel);
		if (newRoundPanel != null)
			this.remove(newRoundPanel);
		if (inGamePanel != null)
			this.remove(inGamePanel);
	}
	
	// on set les paramètres à chaque fois car ils sont perdus quand on change le panel à afficher
	private void resetJFrameParameters() {
		this.setSize(1280,720);
		this.setResizable(false);
		this.setTitle("Jeu du lapin - Baudvin, Cousi, Marquet, Privé, Remise, Pairault");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}


	public static void main(String[] args) {
		//LOGGER.debug("Program started");
		Menu menu = Menu.getInstance();
		menu.switchPanel(Game.MENU);

		return;
	}
}
