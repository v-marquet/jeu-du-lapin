package org.bitbucket.vmarquet.Model;

import org.junit.*;

import java.awt.Dimension;

public class GameModelTest {

	private GameModel gameModel;
	
	@Before
	public void before() {
		gameModel = GameModel.getInstance();
	}
	
	@After
	public void after() {
		
		// Delete the garden
		gameModel = null;
		
		// Call the garbage collector
		System.gc();
		
	}
	
	@Test
	// Test that the number of players added is correct in the model
	public void test1() {
		
		// Arrange
		int nbPlayers = 7;
		
		// Act
		gameModel.setNumberOfPlayers(nbPlayers);
		
		// Assert
		Assert.assertEquals(nbPlayers, gameModel.getNumberOfPlayers());
		
	}
	
	@Test
	// Test that the garden is created
	public void test2() {
		
		// Arrange
		Dimension dimension = new Dimension(4,7);
		
		// Act
		setGeneralGarden(dimension);
		
		// Assert
		Assert.assertEquals(true, gameModel.isGardenSet());
		
	}

	@Test
	// Test that the garden has the proper dimension
	public void test3() {
		
		// Arrange
		Dimension dimension = new Dimension(4,7);
		
		// Act
		setGeneralGarden(dimension);
		
		// Assert
		Assert.assertEquals(dimension, gameModel.getGardenSize());
		
	}
	
	@Test
	// Test that the matrix is well cleared after calling clearGarden()
	public void test4() {
		
		// Arrange
		Dimension dimension = new Dimension(4,7);
		GardenMatrix garden = new GardenMatrix(dimension);
		gameModel.setGardenMatrix(garden);
		
		// Act
		gameModel.clearGarden();
		
		// Assert
		Assert.assertNull(gameModel.getMatrix());
		
	}
	
	@Test
	// Test that the players are properly removed
	public void test5() {
		
		// Arrange
		int nbPlayers = 7;
		gameModel.setNumberOfPlayers(nbPlayers);
		
		// Act
		gameModel.removePlayers();
		
		// Assert
		Assert.assertEquals(0, gameModel.getNumberOfPlayers());
		
	}
	
	@Test
	// Test that we can add a player correctly
	public void test6() {
		
		// Arrange
		Player newPlayer = new Player();
		
		// Act
		gameModel.addPlayer(newPlayer);
		
		// Assert
		Assert.assertEquals(1, gameModel.getNumberOfPlayers());
		
	}
	
	@Test
	// Test that clearNewRound works
	public void test7() {
		
		// Arrange
		Player player;
		int playerNbCarrots;
		
		Dimension dimension = new Dimension(4,7);
		setGeneralGarden(dimension);
		for(int i = 0 ; i < 6 ; i++) {
			player = new Player();
			player.setCarrots(42);
			gameModel.addPlayer(player);
		}
		
		// Act
		gameModel.clearNewRound();
		
		// Assert
		for(int i = 0 ; i < 6 ; i++) {
			playerNbCarrots = gameModel.getPlayerAt(i).getCarrots();
			Assert.assertEquals(0, playerNbCarrots);
		}
		
		
		
	}
	
	// Method to set a general garden
	public void setGeneralGarden(Dimension dimension) {
		
		// Create the garden
		GardenMatrix garden = new GardenMatrix(dimension);
		
		// Apply the garden to the model
		gameModel.setGardenMatrix(garden);
		
	}
	
}
