package org.bitbucket.vmarquet.Model;

import org.junit.*;
import java.awt.Dimension;
import org.bitbucket.vmarquet.Model.GardenMatrix;
public class GardenMatrixTest {
	
	private GardenMatrix garden;
	
	@After
	public void after() {
		
		// Remove the garden
		garden = null;
		
		// Call the garbage collector
		System.gc();
		
	}
	
	@Test
	// Test if the dimensions are correctly loaded in the matrix
	public void isDimensionLoaded() {
		
		// Arrange
		Dimension dimension = new Dimension(4, 5);
		
		// Act
		garden = setGeneralGarden(dimension);
		
		// Assert
		Assert.assertEquals(dimension, garden.getGardenSize());
		
	}
	
	@Test
	// Test if a carrot (same for rock) is correctly loaded in the garden
	public void isCarrotLoaded() {
		
		// Arrange
		Dimension dimension = new Dimension(4, 7);
		garden = setGeneralGarden(dimension);
		
		// Act
		garden.addObject(new Dimension(2, 3), GardenObject.CARROT);
		garden.addObject(new Dimension(1, 1), GardenObject.ROCK);
			
		// Assert
		Assert.assertEquals(GardenObject.CARROT, garden.getObjectsAt(new Dimension(2, 3)).get(0));
		Assert.assertEquals(GardenObject.ROCK, garden.getObjectsAt(new Dimension(1, 1)).get(0));
		
	}

	@Test
	// Test if a carrot is succesfully removed
	public void isCarrotSuppressed() {
		
		// Arrange
		Dimension dimension = new Dimension(4,7);
		garden = setGeneralGarden(dimension);
		
		// Act
		garden.addObject(new Dimension(2, 3), GardenObject.CARROT);
		garden.removeCarrot(new Dimension(2, 3));
		
		// Assert
		Assert.assertEquals(0, garden.getObjectsAt(new Dimension(2, 3)).size());
		
	}
	
	// Method to set a general garden
	public GardenMatrix setGeneralGarden(Dimension dimension) {
		
		// Create the garden
		GardenMatrix garden = new GardenMatrix(dimension);
		
		// Return the garden
		return garden;
		
	}
	
}
