package org.bitbucket.vmarquet.Model;

import org.junit.*;

public class PlayerTest {
	
	private Player player;
	
	@Before
	public void before() {
		
		player = new Player();
	}
	
	@After
	public void after() {
		
		// Remove the player
		player = null;
		
		// Call the garbage collector
		System.gc();
	}
	
	@Test
	// Test if the player is correctly loaded
	public void isPlayerLoaded() {
		
		// Assert
		Assert.assertEquals(0, player.getScore());
		Assert.assertEquals(0, player.getCarrots());
	}
	
	@Test
	// Test if the player's name is correct
	public void isPlayerNameSet() {
		
		// Arrange
		player.setName("BugsBunny");
			
		// Assert
		Assert.assertEquals("BugsBunny", player.getName());
		
	}

	@Test
	// Test if carrots are successfully loaded
	public void isCarrotsLoaded() {
		
		// Arrange
		player.setCarrots(4);
		
		// Assert
		Assert.assertEquals(4, player.getCarrots());
	}
	
	@Test
	// Test if orientation are successfully loaded
	public void isOrientationLoaded() {
		
		// Arrange
		player.setOrientation('N');
		
		// Assert
		Assert.assertEquals('N', player.getOrientation());
	}
	
}