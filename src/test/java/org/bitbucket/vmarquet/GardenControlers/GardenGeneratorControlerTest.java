package org.bitbucket.vmarquet.GardenControlers;

import org.junit.*;

public class GardenGeneratorControlerTest {

	GardenGeneratorControler gardenGenerator;
	
	@Before
	public void before() {
		
		gardenGenerator = new GardenGeneratorControler();
	}
	
	@After
	public void after() {
		
		// Remove the gardenGenerator
		gardenGenerator = null;
		
		// Call the garbage collector
		System.gc();
	}
	
	@Test
	// Test that the garden as a correct size
	public void test1() {
			
		// Assert
		Assert.assertTrue(gardenGenerator.getHeight() > 0);
		Assert.assertTrue(gardenGenerator.getWidth() > 0);
	}
	
}
