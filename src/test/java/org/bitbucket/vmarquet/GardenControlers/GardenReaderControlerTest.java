package org.bitbucket.vmarquet.GardenControlers;

import org.junit.*;

public class GardenReaderControlerTest {
	
	private GardenReaderControler gardenController;
	
	@Before
	public void before() {
		
		gardenController = new GardenReaderControler("./src/test/resources/gardens/gardenTest.txt");
	}
	
	@After
	public void after() {
		
		// Remove the gardenGenerator
		gardenController = null;
		
		// Call the garbage collector
		System.gc();
	}
	
	@Test
	// Test if the gardenReaderControler have correct width and height
	public void isGardenContCorrect() {
		
		// Assert
		Assert.assertEquals(6, gardenController.getWidth());
		Assert.assertEquals(5, gardenController.getHeight());
	}
}

